pragma solidity ^0.4.18;

contract SimpleContract {
    struct Data {
        string hashData;
        string bigchainID;
    }
    
    mapping(address => Data) addressToData;
    
    function setAddressToData(string _hashData, string _bcdbID) public {
        addressToData[msg.sender] = Data(_hashData, _bcdbID);
    }
    
    function getAddressToData() public view returns (string, string) {
        return(addressToData[msg.sender].hashData, addressToData[msg.sender].bigchainID);
    }
}