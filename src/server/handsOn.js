const express = require('express');
const path = require('path');
const passport = require('passport');

var routerhandsOn = express.Router();

routerhandsOn.get('/handsOn', function (req, res) {
	res.sendFile(path.join(__dirname + '/../public/handson/index.html'));
});

const driver = require('bigchaindb-driver')

const alice = new driver.Ed25519Keypair()
const conn = new driver.Connection('https://test.bigchaindb.com/api/v1/',{ 
	app_id: '5e18c229',
	app_key: '22568e24b58d51637d469b716c6050aa' 
})

routerhandsOn.post('/createBlockchainData', function (req, res){
	//createBlockchainDataFn(req, res);
	const{ hello }= req.body;
	console.log(hello);

	const tx = driver.Transaction.makeCreateTransaction(
    	{ message: 'kk' },
    	null,
    	[ driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(alice.publicKey))],
		alice.publicKey
	)
	const txSigned = driver.Transaction.signTransaction(tx, alice.privateKey)
	conn.postTransactionCommit(txSigned)
	.then(retrievedTx => {
		console.log('Transaction', retrievedTx.id, 'successfully posted.')
		res.send("cool");
	}).catch(function(err) {
		console.log(err.message);
		res.send("not cool");
	});
});

module.exports = routerhandsOn;