App = {
    web3Provider: null,
    contracts: {},

    init: function() {
        return App.initWeb3();
    },

    initWeb3: function() {
        if (typeof web3 !== 'undefined') {
            App.web3Provider = web3.currentProvider;
        } else {
        	$('.overlay').removeClass('hidden');
        	return;
            App.web3Provider = new Web3.providers.HttpProvider('http://localhost:9545');
        }
        web3 = new Web3(App.web3Provider);
        return App.initContract();
    },

    initContract: function() {
        $.getJSON('/public/contracts/SimpleContract.json', function(data) {
            var SimpleContractArtifact = data;
            App.contracts.SimpleContract = TruffleContract(SimpleContractArtifact);
            App.contracts.SimpleContract.setProvider(App.web3Provider);
        });
        return App.bindEvents();
    },

    bindEvents: function() {
        $(document).on('click', '.btn-view', App.getData);
        $(document).on('click', '.btn-add', App.setData);
    },

    setData: function() {
        event.preventDefault();
        var simpleInstance;
        var input = {'hello': document.getElementById("inputText").value}

        $.ajax({
            url: '/createBlockchainData',
            type: 'post',
            data: input
        })
        .done(function(res) {
            web3.eth.getAccounts(function(error, accounts) {
                if (error) {
                    console.log(error);
                }
        
                var account = accounts[0];
                console.log(account);
                App.contracts.SimpleContract.deployed().then(function(instance) {
                    simpleInstance = instance;
                    return simpleInstance.setAddressToData(input.hello, input.hello, {from: account});
                }).then(function(result) {
                    console.log(result);
                }).catch(function(err) {
                    console.log(err.message);
                });
            });
        });
    },

    getData: function() {
        event.preventDefault();
        var simpleInstance;
    
        web3.eth.getAccounts(function(error, accounts) {
            if (error) {
                console.log(error);
            }
    
            var account = accounts[0];
            console.log(account);
            App.contracts.SimpleContract.deployed().then(function(instance) {
                simpleInstance = instance;
    
                return simpleInstance.getAddressToData({from: account});
            }).then(function(result) {
                //return App.markAdopted();
                console.log(result);
            }).catch(function(err) {
                console.log(err.message);
            });
        });
    },
};

$(function() {
    $(".preloader").addClass("active");
	setTimeout(function () {
	    $(".preloader").addClass("done");
	}, 1500);
    App.init();
});
